<?php 


require_once 'API.php';

use PHPUnit\Framework\TestCase;


class APITest extends TestCase
{
    protected function setUp(): void
    {
        $this->api = new API();
    }

    public function testHttpPost()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';
        
        $payload = array(
            
            'first_name' => 'Test Name',
            'middle_name' => 'M',
            'last_name' => 'Last test',
            'contact_number' => '4655'
        );
        $result = json_decode($this->api->httpPost($payload), true);
        print_r($result);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
        

        return $result;
    }

    /**
     * @depends testHttpPost
     */
    public function testHttpPut(array $result)
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';
        $id = $result['id'];
        $payload = array(
            
            'first_name' => 'Updated Test',
            'middle_name' => 'updated test',
            'last_name' => 'updated last test',
            'contact_number' => '9876543210'
        );
        $result = json_decode($this->api->httpPut($id, $payload), true);
        print_r($result);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('id', $result);
        $this->assertArrayHasKey('data', $result);
        

        return $result['data'];
    } 

    /**
     * @depends testHttpPost
     */
    public function testHttpGet(array $result)
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $payload = ['id' => $result['id']];

        $result = json_decode($this->api->httpGet($payload), true);
        print_r($result);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);

        return $result['data'];
    }

    /**
     * @depends testHttpPost
     */
    public function testHttpDelete(array $result)
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';

        $id = $result['id'];
        $result = json_decode($this->api->httpDelete($id), true);
        print_r($result);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }
}







?>