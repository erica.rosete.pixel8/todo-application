<?php
 

 header("Access-Control-Allow-Origin: *");
 header("Access-Control-Allow-Credentials: true");
 header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
 header("Access-Control-Allow-Headers: Content-Type");

 require_once('MysqliDb.php');

 class API {
    public function __construct()
    {
        $this->db = new MysqliDB('localhost', 'root', '', 'employee');
    }

    public function httpGet($payload = array())
      {
        if(isset($payload['id'])){
            $id = $payload['id'];
            $query = $this->db->where('id',$id);
        }
        
          // execute query
          $query = $this->db->get( 'information');


          //check if query is success or fail
          if ($query) {
              return json_encode(array(
                  'method' => 'GET',
                  'status' => 'success',
                  'data' => $query,
              ));
          } else {
              return json_encode(array(
                  'method' => 'GET',
                  'status' => 'fail',
                  'data' => [],
                  'message' => 'Failed to Fetch'
              ));
          }
      }


      public function httpPost( $payload)
      {
         
          //Execute Query
          $query = $this->db->insert('information', $payload);
          
          

          //check if query is success or fail
          if ($query) {
            
              return json_encode(array(
                  'method' => 'POST',
                  'status' => 'success',
                  'id' => $this->db->getInsertId(),
                  'data' => $payload,
              ));
          } else {
              return json_encode(array(
                  'method' => 'POST',
                  'status' => 'fail',
                  'data' => [],
                  'message' => 'Failed to Insert'
              ));
          }
      }


      public function httpPut($id, $payload)
{
    

    // where clause
    $this->db->where('id', $id);

    //execute query
    $query = $this->db->update('information', $payload);

    //check if query is success or fail
    if ($query) {
        return json_encode(array(
            'method' => 'PUT',
            'status' => 'success',
            'id' => $id,
            'data' => $payload,
        ));
    } else {
        return json_encode(array(
            'method' => 'PUT',
            'status' => 'fail',
            'data' => [],
            'message' => 'Failed to Update'
        ));
    }
}


      public function httpDelete($id)
      {
           // Explode the ids 
          $selected_id = ['id' => is_string($id) ? explode(",", $id) : null];	


         // Check if there are any selected ids in the $selected_id array
         if ($selected_id['id'] !== null && count($selected_id['id'])) {
        // If there are, use the IN operator to search for those specific ids in the 'id' column
            $this->db->where('id', $selected_id['id'], 'IN');
        } else {
            // If there are no selected ids, use the normal operator to search for the single id in the 'id' column
            $this->db->where('id', $id);
        }




          // Execute query
          $query = $this->db->delete('information');


          // check if success or fail
          if ($query) {
              return json_encode(array(
                  'method' => 'DELETE',
                  'status' => 'success',
                  'data' => [],
              ));
              

          } else {
              return json_encode(array(
                  'method' => 'DELETE',
                  'status' => 'fail',
                  'data' => [],
                  'message' => 'Failed to Delete'
              ));
          }
      }

  }


//   $request_method = $_SERVER['REQUEST_METHOD'];


//  if ($request_method === 'GET') {
//       $received_data = $_GET;
//   } else {
//       //check if method is PUT or DELETE, and get the ids on URL
//       if ($request_method === 'PUT' || $request_method === 'DELETE') {
//           $request_uri = $_SERVER['REQUEST_URI'];


//           $ids = null;
//           $exploded_request_uri = array_values(explode("/", $request_uri));


//           $last_index = count($exploded_request_uri) - 1;


//           $ids = $exploded_request_uri[$last_index];


//           }
//       }

//       //payload data
//       $received_data = json_decode(file_get_contents('php://input'), true);
      

//       $api = new API;


//       //Checking if what type of request and designating to specific functions
//        switch ($request_method) {
//            case 'GET':
//                $api->httpGet($received_data);
//                break;
//            case 'POST':
//                $api->httpPost($received_data);
//                break;
//            case 'PUT':
//                $api->httpPut($ids, $received_data);
//                break;
//            case 'DELETE':
//                $api->httpDelete($ids, $received_data);
//                break;
//        }
  


?>
